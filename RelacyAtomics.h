/***************************************************************************
 *     Copyright (c) 2010, Ronald Landheer-Cieslak & Vlinder Software      *
 *                                                                         *
 * Ownership of Copyright                                                  *
 * ======================                                                  *
 * The copyright in this material (including without limitation the text,  *
 * computer code, artwork, photographs, images, music, audio material,     *
 * video material and audio-visual material) is owned by Ronald            *
 * Landheer-Cieslak and/or Vlinder Software.                               *
 *                                                                         *
 * Copyright license                                                       *
 * =================                                                       *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, version 3 of the License.                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef VLINDER_ATOMICS_RELACYATOMICS_H
#define VLINDER_ATOMICS_RELACYATOMICS_H

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4996)
#endif
#include <relacy/atomic.hpp>
#include <relacy/relacy_std.hpp>
#include <relacy/atomic_fence.hpp>
#ifdef _MSC_VER
#pragma warning(pop)
#endif

namespace Vlinder
{
	namespace Atomics
	{
		struct RelacyAtomics
		{
			enum MemoryOrder {
				memory_order_relaxed__ = rl::mo_relaxed,	// no operation orders memory
				memory_order_consume__ = rl::mo_consume,	// a load operation performs a consume operation on the affected memory location
				memory_order_acquire__ = rl::mo_acquire,	// a load operation performs an acquire operation on the affected memory location
				memory_order_release__ = rl::mo_release,	// a store operation performs a release operation on the affected memory location
				memory_order_acq_rel__ = rl::mo_acq_rel,	// a store operation performs a release operation on the affected memory location;
				// a load operation performs an acquire operation on the affected memory location
				memory_order_seq_cst__ = rl::mo_seq_cst,	// a store operation performs a release operation on the affected memory location;
				// a load operation performs an acquire operation on the affected memory location
			};

			template < typename T >
			struct Atomic
				: public rl::atomic< T >
			{
				Atomic()
				{ /* no-op */ }

				explicit Atomic(T t)
					: rl::atomic< T >(t)
				{ /* no-op */ }

				template < typename D >
				T load(const D & debug_info, MemoryOrder memory_order = memory_order_seq_cst__) const
				{
					return rl::atomic< T >::load((rl::memory_order)memory_order, debug_info);
				}

				template < typename D >
				void store(const D & debug_info, T value, MemoryOrder memory_order = memory_order_seq_cst__)
				{
					return rl::atomic< T >::store(value, (rl::memory_order)memory_order, debug_info);
				}

				template < typename D >
				T exchange(const D & debug_info, T value, MemoryOrder memory_order = memory_order_seq_cst__)
				{
					return rl::atomic< T >::exchange(value, (rl::memory_order)memory_order, debug_info);
				}

				template < typename D >
				bool cas(const D & debug_info, T * expected, T desired, MemoryOrder order = memory_order_seq_cst__)
				{
					return rl::atomic< T >::compare_exchange_strong(*expected, desired, (rl::memory_order)order, debug_info);
				}
				
				template < typename D >
				T fetchAndAdd(const D & debug_info, T value, MemoryOrder order = memory_order_seq_cst__)
				{
					return rl::atomic< T >::fetch_add(value, (rl::memory_order)order, debug_info);
				}
				
				template < typename D >
				void add(const D & debug_info, T value, MemoryOrder order = memory_order_seq_cst__)
				{
					rl::atomic< T >::fetch_add(value, (rl::memory_order)order, debug_info);
				}
				
				template < typename D >
				void sub(const D & debug_info, T value, MemoryOrder order = memory_order_seq_cst__)
				{
					rl::atomic< T >::fetch_sub(value, (rl::memory_order)order, debug_info);
				}
			};

			template < typename D >
			static void membarRelease(const D & debug_info)
			{
				rl::atomic_thread_fence(rl::mo_release, debug_info);
			}

			template < typename D >
			static void membarAcquire(const D & debug_info)
			{
				rl::atomic_thread_fence(rl::mo_acquire, debug_info);
			}

			template < typename D >
			static void assertion(const D & debug_info, bool assertion, const char * assertion_text)
			{
				RL_ASSERT_IMPL(assertion, rl::test_result_user_assert_failed, assertion_text, debug_info);
			}
		};
	}
}

#endif
