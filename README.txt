The PlatformAtomics header of library, for the moment, is largely a C++ wrapper around Boehm's atomics library, of which the sources are included but can also be found at http://www.hpl.hp.com/personal/Hans_Boehm/gc/gc_source/ - inside the source tree of the garbage collector.

The RelacyAtomics header is a wrapper around Relacy (http://groups.google.com/group/relacy).

On Windows, this library is header-only.
On other platforms, you need to build extern/libatomic_ops-1.2. You don't need the malloc implementation or the stack implementation.

I usually install this in <install-root>/include/Vlinder/Atomics

This library is available for commercial use, for a fee. Contact support@vlinder.ca for info.