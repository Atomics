/***************************************************************************
 *     Copyright (c) 2010, Ronald Landheer-Cieslak & Vlinder Software      *
 *                                                                         *
 * Ownership of Copyright                                                  *
 * ======================                                                  *
 * The copyright in this material (including without limitation the text,  *
 * computer code, artwork, photographs, images, music, audio material,     *
 * video material and audio-visual material) is owned by Ronald            *
 * Landheer-Cieslak and/or Vlinder Software.                               *
 *                                                                         *
 * Copyright license                                                       *
 * =================                                                       *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, version 3 of the License.                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef VLINDER_ATOMICS_ATOMICS_H
#define VLINDER_ATOMICS_ATOMICS_H

#if USE_RELACY
#include "RelacyAtomics.h"
#else
#include "PlatformAtomics.h"
#endif

namespace Vlinder
{
	namespace Atomics
	{
#if USE_RELACY
		typedef RelacyAtomics Atomics;
#else
		typedef PlatformAtomics Atomics;
#endif
	}
}

#endif
