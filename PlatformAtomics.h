/***************************************************************************
 *     Copyright (c) 2010, Ronald Landheer-Cieslak & Vlinder Software      *
 *                                                                         *
 * Ownership of Copyright                                                  *
 * ======================                                                  *
 * The copyright in this material (including without limitation the text,  *
 * computer code, artwork, photographs, images, music, audio material,     *
 * video material and audio-visual material) is owned by Ronald            *
 * Landheer-Cieslak and/or Vlinder Software.                               *
 *                                                                         *
 * Copyright license                                                       *
 * =================                                                       *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, version 3 of the License.                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef VLINDER_ATOMICS_PLATFORMATOMICS_H
#define VLINDER_ATOMICS_PLATFORMATOMICS_H

#include <boost/noncopyable.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_pod.hpp>
#if _MSC_VER
#define AO_ASSUME_WINDOWS98
#endif
#include "Private/atomic_ops.h"

#ifndef $
#define $ 0
#endif

namespace Vlinder
{
	namespace Atomics
	{
		struct PlatformAtomics
		{
			enum MemoryOrder {
				memory_order_relaxed__,	// no operation orders memory
				memory_order_consume__,	// a load operation performs a consume operation on the affected memory location
				memory_order_acquire__, // a load operation performs an acquire operation on the affected memory location
				memory_order_release__, // a store operation performs a release operation on the affected memory location
				memory_order_acq_rel__, // a store operation performs a release operation on the affected memory location;
				// a load operation performs an acquire operation on the affected memory location
				memory_order_seq_cst__, // a store operation performs a release operation on the affected memory location;
				// a load operation performs an acquire operation on the affected memory location
			};

			template < typename T >
			struct Atomic
				: public boost::noncopyable
			{
				BOOST_STATIC_ASSERT(sizeof(T) <= sizeof(AO_t));
				BOOST_STATIC_ASSERT(boost::is_pod< T >::value);
#ifdef _MSC_VER
				BOOST_STATIC_ASSERT(sizeof(T) <= sizeof(LONG));
				BOOST_STATIC_ASSERT(sizeof(AO_t) >= sizeof(LONG));
#endif
				Atomic()
				{ /* no-op */ }

				explicit Atomic(T t)
					: value_(t)
				{ /* no-op */ }

				template < typename D >
				T load(const D & debug_info, MemoryOrder memory_order = memory_order_seq_cst__) const
				{
					(void)debug_info;
					switch (memory_order)
					{
					case memory_order_relaxed__ :
						return (T)value_;
					case memory_order_acquire__ :
						return AO_load_acquire(&value_);
					case memory_order_release__ :
					case memory_order_acq_rel__ :
					case memory_order_consume__ :
					case memory_order_seq_cst__ :
					default :
						return (T)AO_load_full(&value_);
					}
				}

				template < typename D >
				void store(const D & debug_info, T value, MemoryOrder memory_order = memory_order_seq_cst__)
				{
					(void)debug_info;
					switch (memory_order)
					{
					case memory_order_relaxed__ :
						value_ = (AO_t)value;
						break;
					case memory_order_acquire__ :
						AO_store_acquire(&value_, (AO_t)value);
						break;
					case memory_order_release__ :
						AO_store_release(&value_, (AO_t)value);
						break;
					case memory_order_acq_rel__ :
					case memory_order_consume__ :
					case memory_order_seq_cst__ :
					default :
						AO_store_full(&value_, (AO_t)value);
						break;
					}
				}

				template < typename D >
				T exchange(const D & debug_info, T value, MemoryOrder memory_order = memory_order_seq_cst__)
				{
					(void)debug_info;
#ifdef _MSC_VER
					switch (memory_order)
					{
					case memory_order_relaxed__ :
					case memory_order_acquire__ :
						InterlockedExchangeAcquire(&value, value);
						break;
					case memory_order_release__ :
					case memory_order_acq_rel__ :
					case memory_order_consume__ :
					case memory_order_seq_cst__ :
					default :
						InterlockedExchange(&value, value);
						break;
					}
#else
					/* This is a very sub-optimal implementation of an atomic exchange, 
					 * but Boehm's library's test-and-set doesn't guarantee that it 
					 * works on T's that are not booleans. */
					T exp((T)value_);
					while (!cas(debug_info, &exp, value, memory_order))
						;
#endif
				}

				template < typename D >
				bool cas(const D & debug_info, T * expected, T desired, MemoryOrder order = memory_order_seq_cst__)
				{
					(void)debug_info;
					switch (memory_order)
					{
					case memory_order_relaxed__ :
					case memory_order_acquire__ :
						if (!AO_compare_and_swap_acquire(&value_, (AO_t)*expected, desired))
						{
							*expected = value_;
							return false;
						}
						else
						{ /* all is well */ }
						return true;
						break;
					case memory_order_release__ :
						if (!AO_compare_and_swap_release(&value_, (AO_t)*expected, desired))
						{
							*expected = value_;
							return false;
						}
						else
						{ /* all is well */ }
						return true;
						break;
					case memory_order_acq_rel__ :
					case memory_order_consume__ :
					case memory_order_seq_cst__ :
					default :
						/* This implementation could re-emit a memory barrier - i.e. if it doesn't work */
						if (!AO_compare_and_swap_full(&value_, (AO_t)*expected, desired))
						{
							*expected = load(debug_info, order);
							return false;
						}
						else
						{ /* all is well */ }
						return true;
						break;
					}
				}
				
				template < typename D >
				T fetchAndAdd(const D & debug_info, T value, MemoryOrder order = memory_order_seq_cst__)
				{
					(void)debug_info;
					switch (order)
					{
					case memory_order_relaxed__ :
					case memory_order_acquire__ :
						return AO_fetch_and_add_acquire(&value_, (AO_t)value);
					case memory_order_release__ :
						return AO_fetch_and_add_release(&value_, (AO_t)value);
					case memory_order_acq_rel__ :
					case memory_order_consume__ :
					case memory_order_seq_cst__ :
					default :
						return AO_fetch_and_add_full(&value_, (AO_t)value);
					}
				}
				
				template < typename D >
				void add(const D & debug_info, T value, MemoryOrder order = memory_order_seq_cst__)
				{
					/* Sub-optimal implementation, but Boehm's library doesn't provide an atomic_add */
					fetchAndAdd(debug_info, value, order);
				}
				
				template < typename D >
				void sub(const D & debug_info, T value, MemoryOrder order = memory_order_seq_cst__)
				{
					/* Sub-optimal implementation, but Boehm's library doesn't provide an atomic_add */
					(void)debug_info;
					switch (memory_order)
					{
					case memory_order_relaxed__ :
					case memory_order_acquire__ :
						return AO_fetch_and_sub_acquire(&value_, (AO_t)value);
					case memory_order_release__ :
						return AO_fetch_and_sub_release(&value_, (AO_t)value);
					case memory_order_acq_rel__ :
					case memory_order_consume__ :
					case memory_order_seq_cst__ :
					default :
						return AO_fetch_and_sub_full(&value_, (AO_t)value);
					}
				}

				mutable AO_t value_;
			};

			static void membarRelease()
			{
				/* Sub-optimal implementation, but Boehm's library doesn't provide a nop_release */
				AO_nop_full();
			}

			static void membarAcquire()
			{
				/* Sub-optimal implementation, but Boehm's library doesn't provide a nop_acquire */
				AO_nop_full();
			}
		};
	}
}

#endif
